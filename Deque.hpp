// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * @param lhs a deque
     * @param rhs a deque
     * @return a bool
     * checks equality
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        // <your code>
        // you must use std::equal()
        return (lhs.size() == rhs.size()) && std::equal(lhs.begin(), lhs.end(), rhs.begin());
    }

    // ----------
    // operator <
    // ----------

    /**
     * @param lhs a deque
     * @param rhs a deque
     * @return a bool
     * checks less than
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        // <your code>
        // you must use std::lexicographical_compare()
        return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----
    // swap
    // ----

    /**
     * @param x a deque
     * @param y a deque
     * swaps two deques
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----

    allocator_type _a;
    allocator_type_2 _a2;

    static const size_type BLOCK_SIZE = 100;
    size_type begin_offset = 0; // Number of possible spaces before first element
    size_type end_offset = 0; // Number of possible spaces after last element
    size_type length = 0; // Current number of elements
    size_type row = 0;
    T** _x = nullptr; // 2D array backing structure
    T* _b = nullptr; // beginning
    T* _e = nullptr; // end
    // <your data>

private:
    // -----
    // valid
    // -----

    bool valid () const {
        // <your code>
        return (!_x && !_b && !_e) || (_x && _b && _e);
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * @param lhs an iterator
         * @param rhs an iterator
         * @return a bool
         * checks equality
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        } // fix

        /**
         * @param lhs an iterator
         * @param rhs an iterator
         * @return a bool
         * checks not equal
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * @param lhs an iterator
         * @param rhs a difference_type
         * @return an iterator
         * adds a difference_type to an iterator
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * @param lhs an iterator
         * @param rhs a difference_type
         * @return an iterator
         * minus a difference_type to an iterator
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----

        // <your data>
        my_deque* _r = nullptr; // pointer to my_deque object
        pointer _p = nullptr; // pointer to current element
        pointer _first = nullptr; // pointer to first element in row
        value_type row_num;
        value_type index;

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
            return ((!_p && !_first) || index <= (value_type)BLOCK_SIZE);
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * @param r a deque pointer
         * @param k a value_type, the row number
         * @param i a value_type, the index
         * @return an iterator
         * Constructs an iterator
         */
        iterator (my_deque* r, value_type k, value_type i) {
            // <your code>
            _r = r;
            row_num = k;
            index = i;
            if((*r)._x != nullptr) {
                _p = &((*r)._x[row_num][index]);
                _first = (*r)._x[row_num];
            }
            assert(valid());
        }

        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * @return a reference
         * Returns the reference of the iterator position
         */
        reference operator * () const {
            // <your code>
            return *_p;
        }

        // -----------
        // operator ->
        // -----------

        /**
         * @return a pointer
         * Returns the pointer to this element
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * @return an iterator reference
         * Pre increments this iterator
         */
        iterator& operator ++ () {
            // <your code>
            if ((index + 1) == BLOCK_SIZE) { // If at end of row
                index = 0;
                if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                    int true_row = 0;
                    while((*_r)._x[true_row] != _first) // Find current row position
                        ++true_row;
                    row_num = true_row;
                }
                _p = &((*_r)._x[++row_num][index]);
                _first = (*_r)._x[row_num];
            } else {
                ++_p;
                ++index;
            }
            assert(valid());
            return *this;
        }

        /**
         * @return an iterator
         * Post increments this iterator
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * @return an iterator reference
         * Pre decrements this iterator
         */
        iterator& operator -- () {
            // <your code>
            if (index == 0) { // If at beginning of row
                index = BLOCK_SIZE - 1;
                if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                    int true_row = 0;
                    while((*_r)._x[true_row] != _first)
                        ++true_row;
                    row_num = true_row;
                }
                --row_num;
                _p = &((*_r)._x[row_num][index]);
                _first = (*_r)._x[row_num];
            } else {
                --_p;
                --index;
            }
            assert(valid());
            return *this;
        }

        /**
         * @return an iterator
         * Post decrements this iterator
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * @param d a difference_type
         * @return an iterator reference
         * Increments this iterator by d
         */
        iterator& operator += (difference_type d) {
            // <your code>
            if (d < 0) {
                *this -= -d;
                return *this;
            }
            int total_index = index + d;
            index = total_index % BLOCK_SIZE;
            if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                int true_row = 0;
                while((*_r)._x[true_row] != _first)
                    ++true_row;
                row_num = true_row;
            }
            row_num = row_num + (total_index / BLOCK_SIZE);
            _p = &((*_r)._x[row_num][index]);
            _first = (*_r)._x[row_num];
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * @param d a difference_type
         * @return an iterator reference
         * Decrements this iterator by d
         */
        iterator& operator -= (difference_type d) {
            // <your code>
            if (d < 0) {
                *this += -d;
                return *this;
            }
            if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                int true_row = 0;
                while((*_r)._x[true_row] != _first)
                    ++true_row;
                row_num = true_row;
            }
            int total_index = (row_num*BLOCK_SIZE + index);
            total_index -= d;
            row_num = total_index / BLOCK_SIZE;
            index = total_index % BLOCK_SIZE;
            _first = (*_r)._x[row_num];
            _p = &((*_r)._x[row_num][index]);
            assert(valid());
            return *this;
        }
    };

public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * @param lhs an iterator
         * @param rhs an iterator
         * @return a bool
         * checks equality
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        } // fix

        /**
         * @param lhs an iterator
         * @param rhs an iterator
         * @return a bool
         * checks not equal
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * @param lhs an iterator
         * @param rhs a difference_type
         * @return an iterator
         * adds a difference_type to an iterator
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * @param lhs an iterator
         * @param rhs a difference_type
         * @return an iterator
         * minus a difference_type to an iterator
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        // <your data>
        const my_deque* _r = nullptr; // pointer to my_deque object
        pointer _p = nullptr; // pointer to current element
        pointer _first = nullptr; // pointer to first element in row
        value_type row_num;
        value_type index;

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
            return ((!_p && !_first) || index <= (value_type)BLOCK_SIZE);
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * @param r a deque pointer
         * @param k a value_type, the row number
         * @param i a value_type, the index
         * @return an iterator
         * Constructs an iterator
         */
        const_iterator (const my_deque* r, value_type k, value_type i) {
            // <your code>
            _r = r;
            row_num = k;
            index = i;
            if((*r)._x != nullptr) {
                _p = &((*r)._x[row_num][index]);
                _first = (*r)._x[row_num];
            }
            assert(valid());
        }

        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * @return a reference
         * Returns the reference of the iterator position
         */
        reference operator * () const {
            // <your code>
            return *_p;
        }           // fix

        // -----------
        // operator ->
        // -----------

        /**
         * @return a pointer
         * Returns the pointer to this element
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * @return an iterator reference
         * Pre increments this iterator
         */
        const_iterator& operator ++ () {
            // <your code>
            if ((index + 1) == BLOCK_SIZE) { // If at end of row
                index = 0;
                if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                    int true_row = 0;
                    while((*_r)._x[true_row] != _first) // Find current row position
                        ++true_row;
                    row_num = true_row;
                }
                _p = &((*_r)._x[++row_num][index]);
                _first = (*_r)._x[row_num];
            } else {
                ++_p;
                ++index;
            }
            assert(valid());
            return *this;
        }

        /**
         * @return an iterator
         * Post increments this iterator
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * @return an iterator reference
         * Pre decrements this iterator
         */
        const_iterator& operator -- () {
            // <your code>
            if (index == 0) { // If at beginning of row
                index = BLOCK_SIZE - 1;
                if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                    int true_row = 0;
                    while((*_r)._x[true_row] != _first)
                        ++true_row;
                    row_num = true_row;
                }
                --row_num;
                _p = &((*_r)._x[row_num][index]);
                _first = (*_r)._x[row_num];
            } else {
                --_p;
                --index;
            }
            assert(valid());
            return *this;
        }

        /**
         * @return an iterator
         * Post decrements this iterator
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * @param d a difference_type
         * @return an iterator reference
         * Increments this iterator by d
         */
        const_iterator& operator += (difference_type d) {
            // <your code>
            if (d < 0) {
                *this -= -d;
                return *this;
            }
            int total_index = index + d;
            index = total_index % BLOCK_SIZE;
            if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                int true_row = 0;
                while((*_r)._x[true_row] != _first)
                    ++true_row;
                row_num = true_row;
            }
            row_num = row_num + (total_index / BLOCK_SIZE);
            _p = &((*_r)._x[row_num][index]);
            _first = (*_r)._x[row_num];
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * @param d a difference_type
         * @return an iterator reference
         * Decrements this iterator by d
         */
        const_iterator& operator -= (difference_type d) {
            // <your code>
            if (d < 0) {
                *this += -d;
                return *this;
            }
            if (row_num >= (*_r).row || &(*_r)._x[row_num] != &_first) { // If row num has changed, find new position
                int true_row = 0;
                while((*_r)._x[true_row] != _first)
                    ++true_row;
                row_num = true_row;
            }
            int total_index = (row_num*BLOCK_SIZE + index);
            total_index -= d;
            row_num = total_index / BLOCK_SIZE;
            index = total_index % BLOCK_SIZE;
            _first = (*_r)._x[row_num];
            _p = &((*_r)._x[row_num][index]);
            assert(valid());
            return *this;
        }
    };

public:
    // ------------
    // constructors
    // ------------

    my_deque () = default;

    /**
     * @param s a size_type
     * Constructs a my_deque
     */
    explicit my_deque (size_type s) :
        _a(),
        _a2() {
        // <your code>
        if (s != 0) {
            length = s;
            if (s <= BLOCK_SIZE) { // s less than 1 block
                _x = _a2.allocate(1);
                row = 1;
                _b = _a.allocate(BLOCK_SIZE);
                _e = _b + s;
                end_offset = BLOCK_SIZE - s;
                _x[0] = _b;
                my_uninitialized_fill(_a, begin(), end(), value_type());
            } else { // need several blocks
                size_type num_blocks = (s / BLOCK_SIZE);
                if (s % BLOCK_SIZE != 0)
                    ++num_blocks;
                _x = _a2.allocate(num_blocks);
                row = num_blocks;
                for (size_type i = 0; i < num_blocks; i++) { // Allocate rest of blocks
                    T* temp = _a.allocate(BLOCK_SIZE);
                    my_uninitialized_fill(_a, temp, temp + BLOCK_SIZE, value_type());
                    _x[i] = temp;
                }
                _b = _x[0];
                if (s % BLOCK_SIZE != 0) {
                    _e = _x[num_blocks - 1] + (s % BLOCK_SIZE);
                    end_offset = BLOCK_SIZE - (s % BLOCK_SIZE);
                }
                else {
                    _e = _x[num_blocks - 1] + BLOCK_SIZE;
                    end_offset = 0;
                }
            }
        } else {
            length = 0;
            _x = _a2.allocate(1);
            row = 1;
            _b = _a.allocate(BLOCK_SIZE);
            _e = _b;
            end_offset = BLOCK_SIZE;
            _x[0] = _b;
        }
        assert(valid());
    }

    /**
     * @param s a size_type
     * @param v a const_reference
     * Constructs a my_deque
     */
    my_deque (size_type s, const_reference v) :
        _a(),
        _a2() {
        // <your code>
        if (s != 0) {
            length = s;
            if (s <= BLOCK_SIZE) { // s less than 1 block
                _x = _a2.allocate(1);
                row = 1;
                _b = _a.allocate(BLOCK_SIZE);
                _e = _b + s;
                end_offset = BLOCK_SIZE - s;
                _x[0] = _b;
                my_uninitialized_fill(_a, begin(), end(), v);
            } else { // need several blocks
                size_type num_blocks = (s / BLOCK_SIZE);
                if (s % BLOCK_SIZE != 0)
                    ++num_blocks;
                _x = _a2.allocate(num_blocks);
                row = num_blocks;
                for (size_type i = 0; i < num_blocks; i++) { // Allocate rest of blocks
                    T* temp = _a.allocate(BLOCK_SIZE);
                    my_uninitialized_fill(_a, temp, temp + BLOCK_SIZE, v);
                    _x[i] = temp;
                }
                _b = _x[0];
                if (s % BLOCK_SIZE != 0) {
                    _e = _x[num_blocks - 1] + (s % BLOCK_SIZE);
                    end_offset = BLOCK_SIZE - (s % BLOCK_SIZE);
                }
                else {
                    _e = _x[num_blocks - 1] + BLOCK_SIZE;
                    end_offset = 0;
                }
            }
        }
        else {
            length = 0;
            _x = _a2.allocate(1);
            row = 1;
            _b = _a.allocate(BLOCK_SIZE);
            _e = _b;
            end_offset = BLOCK_SIZE;
            _x[0] = _b;
        }
        assert(valid());
    }

    /**
     * @param s a size_type
     * @param v a const_reference
     * @param a a const allocator_type reference
     * Constructs a my_deque
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) :
        _a(a),
        _a2(a) {
        // <your code>
        if (s != 0) {
            length = s;
            if (s <= BLOCK_SIZE) { // s less than 1 block
                _x = _a2.allocate(1);
                row = 1;
                _b = _a.allocate(BLOCK_SIZE);
                _e = _b + s;
                end_offset = BLOCK_SIZE - s;
                _x[0] = _b;
                my_uninitialized_fill(_a, begin(), end(), v);
            } else { // need several blocks
                size_type num_blocks = (s / BLOCK_SIZE);
                if (s % BLOCK_SIZE != 0)
                    ++num_blocks;
                _x = _a2.allocate(num_blocks);
                row = num_blocks;
                for (size_type i = 0; i < num_blocks; i++) { // Allocate rest of blocks
                    T* temp = _a.allocate(BLOCK_SIZE);
                    my_uninitialized_fill(_a, temp, temp + BLOCK_SIZE, v);
                    _x[i] = temp;
                }
                _b = _x[0];
                if (s % BLOCK_SIZE != 0) {
                    _e = _x[num_blocks - 1] + (s % BLOCK_SIZE);
                    end_offset = BLOCK_SIZE - (s % BLOCK_SIZE);
                }
                else {
                    _e = _x[num_blocks - 1] + BLOCK_SIZE;
                    end_offset = 0;
                }
            }
        }
        else {
            length = 0;
            _x = _a2.allocate(1);
            row = 1;
            _b = _a.allocate(BLOCK_SIZE);
            _e = _b;
            end_offset = BLOCK_SIZE;
            _x[0] = _b;
        }
        assert(valid());
    }

    /**
     * @param rhs an std::initializer_list<value_type>
     * Constructs a my_deque
     */
    my_deque (std::initializer_list<value_type> rhs) :
        _a(),
        _a2() {
        // <your code>
        my_deque temp(rhs.size());
        my_uninitialized_copy(_a, rhs.begin(), rhs.end(), temp.begin());
        swap(temp);
        assert(valid());
    }

    /**
     * @param rhs an std::initializer_list<value_type>
     * @param a a const allocator_type reference
     * Constructs a my_deque
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) :
        _a(a),
        _a2(a) {
        // <your code>
        my_deque temp(rhs.size());
        my_uninitialized_copy(_a, rhs.begin(), rhs.end(), temp.begin());
        swap(temp);
        assert(valid());
    }

    /**
     * @param that a const my_deque reference
     * Constructs a my_deque
     */
    my_deque (const my_deque& that) :
        _a(that._a),
        _a2(that._a) {
        // <your code>
        if (that.size() != 0) {
            my_deque temp(that.size());
            my_uninitialized_copy(_a, that.begin(), that.end(), temp.begin());
            swap(temp);
        }
        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * Deallocates the current my_deque
     */
    ~my_deque () {
        // <your code>
        if(!empty()) {
            clear();
        }
        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * @return a my_deque reference
     * @param rhs a const my_deque reference
     * A copy constructor
     */
    my_deque& operator = (const my_deque& rhs) {
        // <your code>
        if (this == &rhs)
            return *this;
        if(!empty())
            clear();
        my_deque temp(rhs.size(), value_type(), rhs._a);
        my_uninitialized_copy(temp._a, rhs.begin(), rhs.end(), temp.begin());
        swap(temp);
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * @return a reference
     * @param index a size_type
     * Returns the reference at an index
     */
    reference operator [] (size_type index) {
        // <your code>
        assert(index < size());
        size_type true_index = begin_offset + index;
        return _x[true_index/BLOCK_SIZE][true_index%BLOCK_SIZE];
    }

    /**
     * @return a const_reference
     * @param index a size_type
     * Returns the const reference at an index
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * @return a reference
     * @param index a size_type
     * Returns the reference at an index
     * @throws out_of_range
     */
    reference at (size_type index) {
        // <your code>
        if (index >= size())
            throw std::out_of_range("deque");
        return (*this)[index];
    }

    /**
     * @return a const_reference
     * @param index a size_type
     * Returns the const_reference at an index
     * @throws out_of_range
     */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * @return a reference
     * Returns the reference to the last element
     */
    reference back () {
        // <your code>
        assert(!empty());
        return *(_e - 1);
    }

    /**
     * @return a const_reference
     * Returns the const_reference to the last element
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * @return an iterator
     * Returns the iterator to the first element
     */
    iterator begin () {
        // <your code>
        return iterator(this, 0, begin_offset);
    }

    /**
     * @return a const_iterator
     * Returns the const_iterator to the first element
     */
    const_iterator begin () const {
        // <your code>
        return const_iterator(this, 0, begin_offset);
    }

    // -----
    // clear
    // -----

    /**
     * Clears the deque including deallocation
     */
    void clear () {
        // <your code>
        resize(0);
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * @return a bool
     * Returns true if the deque is empty
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * @return an iterator
     * Returns an iterator to the end of the deque
     */
    iterator end () {
        // <your code>
        return iterator(this, row - 1, BLOCK_SIZE - end_offset);
    }

    /**
     * @return a const_iterator
     * Returns a const_iterator to the end of the deque
     */
    const_iterator end () const {
        // <your code>
        return const_iterator(this, row - 1, BLOCK_SIZE - end_offset);
    }

    // -----
    // erase
    // -----

    /**
     * @return an iterator
     * @param position an iterator
     * Returns an iterator at the erased position
     */
    iterator erase (iterator position) {
        // <your code>
        assert(!empty());
        iterator ahead = position;
        iterator behind = position;
        ++behind;
        while (ahead != --end()) { // Shift elements back
            *ahead = *behind;
            ++ahead;
            ++behind;
        }
        if (end_offset == BLOCK_SIZE - 1) {
            T** _x_temp = _a2.allocate(row - 1);
            my_uninitialized_copy(_a2, &(_x[0]), &(_x[row - 1]), &(_x_temp[0]));
            std::swap(_x, _x_temp);
            end_offset = 0;
            --row;
            _e = &(_x[row - 1][BLOCK_SIZE - 1]) + 1;
        } else {
            --_e;
            ++end_offset;
        }
        --length;
        assert(valid());
        return position;
    }

    // -----
    // front
    // -----

    /**
     * @return a reference
     * Returns a reference to the front
     */
    reference front () {
        // <your code>
        assert(!empty());
        return *_b;
    }

    /**
     * @return a const_iterator
     * Returns a const_iterator to the front of the deque
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * @return an iterator
     * @param position an iterator
     * @param v a const_reference
     * Inserts an element at the given position
     */
    iterator insert (iterator position, const_reference v) {
        // <your code>
        if(end_offset == 0) { // at end, need to add block
            T** _x_temp = _a2.allocate(row + 1);
            _x_temp[row] = _a.allocate(BLOCK_SIZE);
            my_uninitialized_copy(_a2, &(_x[0]), &(_x[row]), &(_x_temp[0]));
            std::swap(_x, _x_temp);
            end_offset = BLOCK_SIZE;
            _e = &(_x[row][1]);
            ++row;
        } else {
            ++_e;
            --end_offset;
        }
        iterator ahead = end();
        iterator behind = end();
        --ahead;
        while (behind != position) { // Shift elements back
            *behind = *ahead;
            --ahead;
            --behind;
        }
        *behind = v;
        ++length;
        assert(valid());
        return position;
    }

    // ---
    // pop
    // ---

    /**
     * Removes the last element
     */
    void pop_back () {
        // <your code>
        assert(!empty());
        if (end_offset + 1 == BLOCK_SIZE) { // If at beginning of row
            T** _x_temp = _a2.allocate(row - 1);
            my_uninitialized_copy(_a2, &(_x[0]), &(_x[row - 1]), &(_x_temp[0]));
            std::swap(_x, _x_temp);
            end_offset = 0;
            --row;
            _e = &(_x[row][BLOCK_SIZE]);
        } else { // Just decrement
            _e = my_destroy(_a, _e - 1, _e);
            ++end_offset;
            _e = _e - 1;
        }
        --length;
        assert(valid());
    }

    /**
     * Removes the first element
     */
    void pop_front () {
        // <your code>
        assert(!empty());
        if (begin_offset + 1 == BLOCK_SIZE) { // If at end of row
            T** _x_temp = _a2.allocate(row - 1);
            my_uninitialized_copy(_a2, &(_x[1]), &(_x[row]), &(_x_temp[0]));
            std::swap(_x, _x_temp);
            begin_offset = 0;
            _b = &(_x[0][0]);
            --row;
        } else { // Just decrement
            my_destroy(_a, _b, _b + 1);
            ++begin_offset;
            _b = _b + 1;
        }
        --length;
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * @param v a const_reference
     * Adds an element at the end
     */
    void push_back (const_reference v) {
        // <your code>
        if (length > 0) { // Not empty
            if (end_offset > 0) { // No need to add row
                my_uninitialized_fill(_a, end(), end() + 1, v);
                _e = _e + 1;
                --end_offset;
            } else { // Add row
                T** _x_temp = _a2.allocate(row + 1);
                _x_temp[row] = _a.allocate(BLOCK_SIZE);
                my_uninitialized_copy(_a2, &(_x[0]), &(_x[row]), &(_x_temp[0]));
                std::swap(_x, _x_temp);
                end_offset = BLOCK_SIZE - 1;
                _e = &(_x[row][BLOCK_SIZE - end_offset]);
                ++row;
                my_uninitialized_fill(_a, end() - 1, end(), v);
            }
            ++length;
        } else { // Empty
            my_deque rhs(0, value_type(), _a);
            swap(rhs);
            my_uninitialized_fill(_a, end(), end() + 1, v);
            _e = _e + 1;
            --end_offset;
            ++length;
        }
        assert(valid());
    }

    /**
     * @param v a const_reference
     * Adds an element at the beginning
     */
    void push_front (const_reference v) {
        // <your code>
        if (length > 0) { // Not empty
            if (begin_offset > 0) { // No need to add row
                _b = _b - 1;
                --begin_offset;
                my_uninitialized_fill(_a, begin(), begin() + 1, v);
            } else { // Add row
                T** _x_temp = _a2.allocate(row + 1);
                _x_temp[0] = _a.allocate(BLOCK_SIZE);
                my_uninitialized_copy(_a2, &(_x[0]), &(_x[row]), &(_x_temp[1]));
                std::swap(_x, _x_temp);
                begin_offset = BLOCK_SIZE - 1;
                _b = &(_x[0][begin_offset]);
                my_uninitialized_fill(_a, begin(), begin() + 1, v);
                ++row;

            }
            ++length;
        } else { // Empty
            my_deque rhs(0, value_type(), _a);
            swap(rhs);
            my_uninitialized_fill(_a, end(), end() + 1, v);
            _e = _e + 1;
            --end_offset;
            ++length;
        }
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * @param s a size_type
     * Changes this deque to a select size
     */
    void resize (size_type s) {
        // <your code>
        if (s == size()) // Same size, do nothing
            return;
        if (s == 0) {
            for(int i = row - 1; i >= 0; i--) {
                _a.deallocate(_x[i], BLOCK_SIZE);
            }
            _a2.deallocate(_x, row);
            length = 0;
            begin_offset = 0;
            end_offset = 0;
            row = 0;
            _x = nullptr;
            _b = nullptr;
            _e = nullptr;
        } else if(s < size()) { // Lowering size
            size_type num_destroy = length - s;
            for (int i = 0; i < num_destroy; i++) {
                pop_back();
            }
        } else if (s <= size() + end_offset) { // Increase size, but still in capacity
            size_type amount_increased = s - size();
            my_uninitialized_fill(_a, _e, _e + amount_increased, value_type());
            end_offset -= amount_increased;
            _e = _e + amount_increased;
        } else { // Increase size and capacity
            size_type increase_amount = s - (length + end_offset);
            size_type update_row = row + increase_amount/BLOCK_SIZE;
            my_uninitialized_fill(_a, end(), end() + end_offset, value_type());
            if (increase_amount % BLOCK_SIZE != 0)
                ++update_row;
            T** _x_temp = _a2.allocate(update_row);
            my_uninitialized_copy(_a2, &(_x[0]), &_x[0] + row, &(_x_temp[0]));
            size_type temp_row = row;
            while(row != update_row) {
                T* temp_block = _a.allocate(BLOCK_SIZE);
                _x_temp[row] = temp_block;
                ++row;
            }
            std::swap(_x, _x_temp);
            my_uninitialized_fill(_a, iterator(this, temp_row, 0), iterator(this, temp_row, 0) + increase_amount, value_type());
            end_offset = BLOCK_SIZE - increase_amount % BLOCK_SIZE;
            _e = &(_x[row - 1][BLOCK_SIZE - end_offset]);
        }
        length = s;
        assert(valid());
    }

    /**
     * @param s a size_type
     * @param v a const_reference
     * Changes this deque to a select size, if larger add elements v
     */
    void resize (size_type s, const_reference v) {
        if (s == size()) // Same size, do nothing
            return;
        if (s == 0) {
            for(int i = row - 1; i >= 0; i--) {
                _a.deallocate(_x[i], BLOCK_SIZE);
            }
            _a2.deallocate(_x, row);
            length = 0;
            begin_offset = 0;
            end_offset = 0;
            row = 0;
            _x = nullptr;
            _b = nullptr;
            _e = nullptr;
        } else if(s < size()) { // Lowering size
            size_type num_destroy = length - s;
            for (int i = 0; i < num_destroy; i++) {
                pop_back();
            }
        } else if (s <= size() + end_offset) { // Increase size, but still in capacity
            size_type amount_increased = s - size();
            my_uninitialized_fill(_a, _e, _e + amount_increased, v);
            end_offset -= amount_increased;
            _e = _e + amount_increased;
        } else { // Increase size and capacity
            size_type increase_amount = s - (length + end_offset);
            size_type update_row = row + increase_amount/BLOCK_SIZE;
            my_uninitialized_fill(_a, end(), end() + end_offset, v);
            if (increase_amount % BLOCK_SIZE != 0)
                ++update_row;
            T** _x_temp = _a2.allocate(update_row);
            my_uninitialized_copy(_a2, &(_x[0]), &_x[0] + row, &(_x_temp[0]));
            size_type temp_row = row;
            while(row != update_row) {
                T* temp_block = _a.allocate(BLOCK_SIZE);
                _x_temp[row] = temp_block;
                ++row;
            }
            std::swap(_x, _x_temp);
            my_uninitialized_fill(_a, iterator(this, temp_row, 0), iterator(this, temp_row, 0) + increase_amount, v);
            end_offset = BLOCK_SIZE - increase_amount % BLOCK_SIZE;
            _e = &(_x[row - 1][BLOCK_SIZE - end_offset]);
        }
        length = s;
        assert(valid());
    }

    // ----
    // size
    // ----

    /**
     * @return a size_type
     * Returns the size of the my_deque
     */
    size_type size () const {
        // <your code>
        return length;
    }

    // ----
    // swap
    // ----

    /**
     * @param rhs a my_deque reference
     * Swaps two my_deque objects
     */
    void swap (my_deque& rhs) {
        // <your code>
        if (_a == rhs._a) {
            std::swap(_x, rhs._x);
            std::swap(_b, rhs._b);
            std::swap(_e, rhs._e);
            std::swap(begin_offset, rhs.begin_offset);
            std::swap(end_offset, rhs.end_offset);
            std::swap(length, rhs.length);
            std::swap(row, rhs.row);
        } else {
            my_deque temp(*this);
            *this = rhs;
            rhs = temp;
        }
        assert(valid());
    }
};

#endif // Deque_h
